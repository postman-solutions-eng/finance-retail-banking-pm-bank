# Finance Retail Banking PM Bank

Repository to store Finance demo environment API versions from the *[Retail Banking] PM Bank Workspace -> Payments API

**The corresponding Postman workspace is part of a set of demo workspaces the Postman Solutions Engineering team put together to show how different industries can utilize workspaces to suit their needs, and does not represent a real company. You can read more about the process of creating this workspace in the companion [blog post](https://blog.postman.com/postman-enterprise-finance-and-retail-industries-api/)**.   

## Welcome to the PM Bank Postman Workspace! 
Our Bank Account API helps introduces the ability for user management and money transfer services via our APIs.

![finance_ov_1](https://user-images.githubusercontent.com/60015240/146238619-8e212e6a-e306-426f-b50e-e6b00375095e.jpeg)

The main benefit of PM Bank API is the elimination of unnecessary procedures from the banking process. In other words, it ensures speed and ease-of-use for financial service providers.

The PM Bank APIs significantly reduce the development time to build new systems or new customer-facing apps that maximize productivity.

Through the PM Bank Workspace, you can retrieve account and statement data from your users and visualize the responses using Postman's Visualizer function.
![Imgur](https://i.imgur.com/WbEHxDz.jpg)

Reference video below to select your Environment and use Visualizer

![Imgur](https://i.imgur.com/ymcwlzB.gif)

